import java.util.Arrays;

public class CheckIfCharsInString {


    public String checkCharsInString(String myString, String myFirstSubstring, String mySecondSubstring) {
        // creating arrays that contains the substrings in my text
        String toLowerText = myString.toLowerCase();
        String[] firstStringToChar = toLowerText.split(myFirstSubstring);
        String firstArrayToString = Arrays.toString(firstStringToChar);
        System.out.println(firstArrayToString);
        String[] secondStringToChar = firstArrayToString.split(mySecondSubstring);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < secondStringToChar.length; i++) {
            stringBuilder.append(secondStringToChar[i]);
        }

        System.out.println(stringBuilder.toString());
        System.out.println();
        return stringBuilder.toString();
    }

    public String secondWayToRemoveSubstring(String myString, String myFirstSubstring, String mySecondSubstring){
        String newString = myString.replace(myFirstSubstring, " ");
        System.out.println(newString);
        String lastString = newString.replace(mySecondSubstring, "");
        lastString = lastString.replace(" ", "");
        System.out.println(lastString);
        return lastString;
    }

}
