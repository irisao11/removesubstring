import java.util.Arrays;

public class CheckIfCharsInStringMain {
    public static void main(String[] args) {
        CheckIfCharsInString checkIfCharsInString = new CheckIfCharsInString();
        checkIfCharsInString.checkCharsInString("abcdacbacf", "b", "ac");
        checkIfCharsInString.secondWayToRemoveSubstring("abcdacbacf", "b", "ac");
    }
}
