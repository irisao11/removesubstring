public class MyString {
    private String myString;

    public MyString(){
    }

    public MyString(String myString) {
        this.myString = myString;
    }

    public String getMyString() {
        return myString;
    }

    public void setMyString(String myString) {
        this.myString = myString;
    }


}
